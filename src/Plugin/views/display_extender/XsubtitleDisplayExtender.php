<?php

namespace Drupal\xsubtitle\Plugin\views\display_extender;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display_extender\DisplayExtenderPluginBase;

/**
 * @ViewsDisplayExtender(
 *   id = "xsubtitle_display_extender",
 *   title = @Translation("Subtitle display extender"),
 *   no_ui = FALSE,
 * )
 */
class XsubtitleDisplayExtender extends DisplayExtenderPluginBase {

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    if ($form_state->get('section') == 'title') {
      $form['subtitle'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Subtitle'),
        '#default_value' => $this->options['subtitle'],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state): void {
    if ($form_state->get('section') == 'title') {
      $this->options['subtitle'] = $form_state->getValue('subtitle');
    }
  }

}
