<?php

namespace Drupal\xsubtitle;

use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\views\Entity\View;

class XsubtitleResolver {

  protected EntityTypeManagerInterface $entityTypeManager;

  protected mixed $subtitle = '';

  /**
   * Service constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Set subtitle.
   */
  public function setSubtitle(mixed $subtitle): void {
    $this->subtitle = $subtitle;
  }

  /**
   * Get subtitle.
   */
  public function getSubtitle() {
    return $this->subtitle;
  }

  /**
   * Return subtitle from route.
   */
  public function getSubtitleFromRoute(RouteMatchInterface $route_match) {
    if ($current_entity = $this->getCurrentEntity($route_match)) {
      return $this->getSubtitleFromEntity($current_entity);
    }

    $current_route_name = $route_match->getRouteName();
    if (str_starts_with($current_route_name, 'view.')) {
      $route_parameters = $route_match->getParameters()->all();
      if (isset($route_parameters['view_id'], $route_parameters['display_id'])) {
        $view_entity = View::load($route_parameters['view_id']);
        $display = $view_entity->getDisplay($route_parameters['display_id']);
        return $display['display_options']['display_extenders']['xsubtitle_display_extender']['subtitle'] ?? NULL;
      }
    }

    return NULL;
  }

  /**
   * Return entity subtitle.
   */
  public function getSubtitleFromEntity(EntityInterface $entity) {
    $subtitle = '';

    if ($entity instanceof FieldableEntityInterface) {
      foreach (['field_subtitle', 'field_page_subtitle'] as $field_name) {
        if ($entity->hasField($field_name)) {
          if ($subtitle_item = $entity->get($field_name)->first()) {
            $subtitle = $subtitle_item->view();
          }
          break;
        }
      }
    }
    elseif ($entity instanceof ThirdPartySettingsInterface) {
      $subtitle = $entity->getThirdPartySetting('xsubtitle', 'subtitle');
    }


    return $subtitle ?: NULL;
  }

  /**
   * Return current entity from route.
   */
  protected function getCurrentEntity(RouteMatchInterface $route_match): ?EntityInterface {
    if (preg_match('/^entity\.(.+)\.canonical$/', $route_match->getRouteName(), $matches)) {
      $entity_type_id = $matches[1];
      $entity = $route_match->getParameter($entity_type_id); /** @var EntityInterface $entity */

      if ($entity && is_numeric($entity)) {
        $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity);
      }

      return $entity;
    }

    return NULL;
  }

}
