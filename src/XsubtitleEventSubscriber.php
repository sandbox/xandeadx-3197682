<?php

namespace Drupal\xsubtitle;

use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class XsubtitleEventSubscriber implements EventSubscriberInterface {

  protected RouteMatchInterface $routeMatch;

  protected XsubtitleResolver $subtitleResolver;

  /**
   * Constructor.
   */
  public function __construct(RouteMatchInterface $route_match, XsubtitleResolver $subtitle_resolver) {
    $this->routeMatch = $route_match;
    $this->subtitleResolver = $subtitle_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::VIEW][] = ['onKernelView', 1];
    return $events;
  }

  /**
   * KernelEvents::VIEW event callback.
   */
  public function onKernelView(ViewEvent $event): void {
    $subtitle = $this->subtitleResolver->getSubtitle();
    if (!$subtitle) {
      $subtitle = $this->subtitleResolver->getSubtitleFromRoute($this->routeMatch);
    }
    if ($subtitle) {
      $this->subtitleResolver->setSubtitle($subtitle);
    }
  }

}
